import { Component,OnInit } from '@angular/core';
import {UserAccessService} from '../services/useracces.service';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-view1',
  templateUrl: './view1.component.html',
  styleUrls:['./view1.component.css']
})
export class ViewOneComponent implements OnInit {
  showSubmenu:boolean = false;
  textvalue:any;
  tabs = [
    {
      tabName: 'register',
      id: 1
    },
    {
      tabName: 'list',
      id: 2
    },
    {
      tabName:'TreeView',
      id:3
    }
  ]
  currentTab: any;
  currentTabIndex: any;
  constructor(private service:UserAccessService,private router:Router){
  }


ngOnInit(){

}

onTabClick(redirectpath){
  this.router.navigateByUrl(`/view1/${redirectpath}`);
}


onLogoutClick(){
  this.service.signout();
  this.router.navigateByUrl(`/login`);
}

 
}
