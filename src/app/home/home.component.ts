import { Component, ElementRef,OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {FormGroup,FormControl,Validators,FormArray, Form, FormBuilder} from '@angular/forms';
import {UserAccessService} from '../services/useracces.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  registerForm:FormGroup;
  submitted: boolean = false;
  constructor(private fb:FormBuilder,private userservice:UserAccessService,private router:Router){

  }

  ngOnInit(){
    this.registerForm=this.fb.group({
      "name": ['', Validators.required],
      "password":['',Validators.required],
    })
  }

 
  get f() { 
    return this.registerForm.controls; 
  }
  onLoginClick(){
    let json={
      name:this.registerForm.get("name").value,
      password:this.registerForm.get("password").value
    }
    this.userservice.signin(json).subscribe(res=>{
      if(res["success"] == true){
      this.router.navigateByUrl(`/view1/register`);
      }else{
         this.router.navigateByUrl(`/login`);
      }
    },error=>{
      console.log("error Occur");
      })
    
  }
}
