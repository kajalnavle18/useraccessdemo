import { Component,OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators,FormArray, Form, FormBuilder} from '@angular/forms';
import {UserAccessService} from '../services/useracces.service';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{
    registerForm:FormGroup;
    submitted: boolean = false;
    userid:any;
    dataOfIndustry: any;
    subDataofIndustry: any;
    constructor(private fb:FormBuilder,private userservice:UserAccessService,private activeroute:ActivatedRoute,private router:Router){
        this.activeroute.paramMap.subscribe(
            params => {
                 this.userid = params["params"].id;
      
            });
    }
    async ngOnInit()
{
    this.registerForm=this.fb.group({
        "name": ['', Validators.required],
        "address":['',Validators.required],
        "email": ['', [Validators.required, Validators.email]],
        "password":['',Validators.required],
        "industry":['',Validators.required],
        "role":['',Validators.required]
      })

      if(this.userid != undefined){
       await  this.getEditData();
       await this.onChangeIndustry();

  
      }
   
      this.getDataOfIndustry();
    

      
}

getEditData(){
    return new Promise((resolve,reject)=>{
        this.userservice.get(`/listOfUser/${this.userid}`).subscribe(result=>{
            
            this.registerForm.patchValue({
               "name": result.payload[0].name,
               "address":result.payload[0].address,
               "email":result.payload[0].email,
               "password":result.payload[0].password,
               "industry":result.payload[0].industry,
               "role":result.payload[0].role
            })
            resolve();
      },error=>{
          reject(error)
      })
    

})
}
get f() { 
    return this.registerForm.controls; 
  }
  onSubmitData(){
    this.submitted = true;
    var json;
    if (this.registerForm.invalid) {
      this.submitted =false;
    }else{
        json={
            "name":this.registerForm.get("name").value,
            "address":this.registerForm.get("address").value,
            "industry":this.registerForm.get("industry").value,
            "role":this.registerForm.get("role").value,
            "email":this.registerForm.get("email").value,
            "password":this.registerForm.get("password").value,
            "id":this.userid
        }
        this.userservice.post("/userregister",json).subscribe(result=>{
            if(result.success){
                this.router.navigateByUrl(`/view1/list`);
            }else{
            this.router.navigateByUrl('/view1/register');
            }
        },error=>{
            console.log("error Occur");
            })
    }
}

getDataOfIndustry(){
    this.userservice.get("/listofIndustry").subscribe(result=>{
        if(result.success){
            this.dataOfIndustry = result.payload;
            console.log("dataOfIndustry",this.dataOfIndustry);
        }else{
            this.dataOfIndustry ='';
        }
      
    },error=>{
    console.log("error Occur");
    })
}

onChangeIndustry(){
    return new Promise((resolve,reject)=>{
        this.userservice.get(`/listofIndustryrole/${this.registerForm.get("industry").value}`).subscribe(result=>{
            if(result.success){
            this.subDataofIndustry = result.payload;
            }else{
                this.subDataofIndustry='';
            }
    resolve();
        },error=>{
            console.log("error Occur");
            reject('error ocur');
            })
    })
    
}
  
}
