import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ViewOneComponent} from './view1/view1.component';
import {UserListComponent} from './userlist/userlist.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {treeviewComponent} from './treeview/treeview.component';
import { 
  AuthGuardService as AuthGuard 
} from './helper/auth.gurd';


const routes: Routes = [
  {
path:'',
component:HomeComponent
  },
  {
    path:'login',
    component:HomeComponent,
    
      },

  {
    path:'view1',
    component:ViewOneComponent,
    children: [
      {
        path: 'register',
        component: RegisterComponent,
        canActivate: [AuthGuard] 
      },
      {
        path: 'register/:id',
        component: RegisterComponent
      },
      {
        path:'list',
        component:UserListComponent,
        canActivate: [AuthGuard] 
      },{
        path:'TreeView',
        component:treeviewComponent
      },
     
    ]
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
