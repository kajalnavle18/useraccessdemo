import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ViewOneComponent} from './view1/view1.component';
import {UserListComponent} from './userlist/userlist.component';
import {HomeComponent} from './home/home.component';
import {viewServices} from './services/view.service';
import {UserAccessService} from './services/useracces.service';
import {RegisterComponent} from './register/register.component';
import {AuthHelper} from './helper/auth';
import {ParamsHelper} from './helper/params';
import {HttpClientModule} from '@angular/common/http';
import{AuthGuardService} from './helper/auth.gurd';
import { Routes, RouterModule } from '@angular/router';

// import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import {MatInput} from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import { TreeviewModule } from 'ngx-treeview';
import {treeviewComponent} from './treeview/treeview.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewOneComponent,
    HomeComponent,
    UserListComponent,
    RegisterComponent,
    treeviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatSliderModule,
    MatCardModule,
    MatSelectModule,
    MatButtonModule,
    MatTabsModule,
    MatTableModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TreeviewModule.forRoot()

  ],
  providers: [viewServices,UserAccessService,ParamsHelper,AuthGuardService,AuthHelper],
  bootstrap: [AppComponent]
})
export class AppModule { }
