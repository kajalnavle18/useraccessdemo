import { Component,OnInit } from '@angular/core';
import { elementAt } from 'rxjs/operators';
import {UserAccessService} from '../services/useracces.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html'
})
export class UserListComponent implements OnInit{
  passValue:any;
  dataSource:any=[];
  ELEMENT_DATA:any=[];
  displayedColumns: string[] = ['name', 'email', 'address','delete','edit'];
  temp:any;
  flagFortable:boolean =false;
  constructor(private service:UserAccessService,private router:Router){

  }

  ngOnInit(){
   this.temp = JSON.parse(localStorage.getItem('currentUser'));
    this.getListOfUser();
  }

  getListOfUser(){
    this.service.get("/listOfUser/0").subscribe(result=>{
      if(result.success){
        this.ELEMENT_DATA.push(result.payload);
        this.ELEMENT_DATA[0].forEach(element => {
          console.log("eleme==>",this.temp[0].industry,element)
          if(element.industry == this.temp[0].industry){
            this.dataSource.push(element);
            this.flagFortable =true;
          }
          
        });
        console.log("datsource==>",this.dataSource)
        // this.dataSource  = this.ELEMENT_DATA[0]
      }else{
        this.dataSource ='';
      }
    },error=>{
      console.log("error Occur");
      })
  }
  onDeleteClick(id){
    this.service.get(`/deleteUser/${id}/${0}`).subscribe(res=>{
      if(res.success){
      location.reload();
      }else{
this.router.navigateByUrl('/view/register')
      }
    },error=>{
      console.log("error Occur");
      })
  }
  onEditClick(id){
    this.router.navigateByUrl(`/view1/register/${id}`);
  }
}
