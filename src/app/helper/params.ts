
import {HttpHeaders} from '@angular/common/http';
import {AuthHelper} from './auth';
import {Router} from '@angular/router';
import { Injectable, NgZone } from '@angular/core';

@Injectable()
export class ParamsHelper {
//   headers: HttpHeaders;
//    router:Router


  constructor(private router:Router) {

  }

  make(body = null, p) {
    let temp: any;
    temp = body;
    if (p != null) {
      return this._append(temp, p);
    }

    return temp;
  }

  /**
   *
   * @param temp
   * @param p
   */
  _append(temp, p) {

    Object.keys(p).forEach(k => {
      const t = temp.append(k, p[k]);
      // below line is important because temp only return
      // it does not update temp
      temp = t;
    });
    return temp;

  }

  makeHeadersWithAuth() {
    const auth = new AuthHelper();
    console.log("-->",auth.getUser().auth_token == "");
    if(auth.getUser().auth_token == ""){
        // this.router.navigateByUrl(`/login`);
    }else{
    const token = auth.getUser()[0].auth_token;
    const headers = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    return headers;
    }
  }

}
