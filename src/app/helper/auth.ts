export class AuthHelper {

    constructor() {
    }
  
    public isLoggedIn(): boolean {
      let temp: any;
      temp = JSON.parse(localStorage.getItem('currentUser'));
      temp = temp;
      if (temp !== undefined) {
        if (temp.id !== undefined) {
          if (temp.auth_token !== undefined || temp.auth_token !== '') {
            return true;
          }
        }
      }
      return false;
    }
  
    public getUser(): any {
      let temp: any;
      if(localStorage.getItem('currentUser') != 'undefined'){
      temp = JSON.parse(localStorage.getItem('currentUser'));
      if (temp != null) {
        console.log("temp==>",temp);
        return temp;
      }
    } else {
        // set default object if not logged, else it give undefined property error on landing page
        return false;
      }
      
    }
  
    public setUser(newData: any): any {
      let temp: any;
      // if we do temp = [] below, it will not work
      // because [] makes it an array
      temp = newData;
      localStorage.setItem('currentUser', JSON.stringify(temp));
    }
  
    public getLocale(): string {
      let temp: any;
      temp = JSON.parse(localStorage.getItem('currentUser'));
      temp = temp.data;
      if (temp.locale === undefined || temp.locale === '') {
        return 'nb';
      }
      return temp.locale;
    }
  
    
  }
  