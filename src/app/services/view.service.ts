import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';


@Injectable()
export class viewServices{

    private viewdata  = new BehaviorSubject(false);
    viewonePassData = this.viewdata.asObservable();


    onViewOnePassData(data){
        this.viewdata.next(data);
      }

}