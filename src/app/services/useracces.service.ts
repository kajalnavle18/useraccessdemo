import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ParamsHelper } from '../helper/params';
import {HttpFormEncodingCodec} from '../helper/http.form.codec';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class UserAccessService {

    headers: HttpHeaders;

   
    constructor(
        private paramsHelper: ParamsHelper,
        private http: HttpClient,
        private _zone: NgZone
    ) {

    }

    /**
     *
     */
    _getBody() {
       return new HttpParams({ encoder: new HttpFormEncodingCodec() });
    }

  
    post(path, p = null): Observable<any> {
        const body = this.paramsHelper.make(this._getBody(), p);
        // console.log('before', body);
        return this.http.post(environment.server + path, body, { headers: this.paramsHelper.makeHeadersWithAuth() })
            .pipe(retry(1),
                catchError(this.handleError), map(data => {
                    return data;
                }));
    }

   

    get(url): Observable<any> {
        return this.http.get(environment.server + url, { headers: this.paramsHelper.makeHeadersWithAuth() })
            .pipe(retry(1),
                catchError(this.handleError), map(data => {
                    return data;
                }));
    }



  
    // Error handling
    handleError(error) {
        return throwError(error);
    }

    signin(p) {
        const body = this.paramsHelper.make(this._getBody(), p);
       
        return this.http.post(environment.server + '/loginUser', body, { headers: this.headers })
            .pipe(map(data => {
                let temp: any;
                 temp = data;
                localStorage.setItem('currentUser', JSON.stringify(temp.payload));
                return data;
                
            }));
            
    }
    signout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
